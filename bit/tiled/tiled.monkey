#Rem monkeydoc Module bit.tiled

This module loads native Tiled files.

Copyright (c) 2015 Eric Canales

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Last updated 2016.

---
Eric Canales~n
eric\@canales.me

#End

#TEXT_FILES+="|*.tmx|*.tsx"

Import skn3.xml
Import mojo.app
Import brl.filepath
Import skid_data_encoders

Class Tiled<I>

	Public
	
	#Rem monkeydoc
	filePath should point to a valid Tiled tmx file. Currently you must set Tiled to save map data as csv, xml or base64 uncompressed.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	</pre>
	#End
	Method New(mapFilePath:String, stopOnError:Bool = False)
		Self.StopOnError = stopOnError
		Self.FilePath = mapFilePath
		
		Local xmlError:= New XMLError
		
		Local mapFile:= LoadString(Self.FilePath)
		
		If mapFile = ""
			Error "Error loading file at location: " + Self.FilePath
		EndIf
		
		Local xml:= ParseXML(mapFile, xmlError)
			
		If xml = Null And xmlError.error
			'error
			If Self.StopOnError = True
				Error xmlError.ToString()
			Else
				DebugLog xmlError.ToString()
			EndIf
			
			Return
		EndIf
		
		'success
		'Read map data
		Self.Map = Self.xmlReadMap(xml)
		
'		DebugStop
	End
	
	#Rem monkeydoc
	Stores the file path to the map file.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.FilePath()
	</pre>
	#End
	Method FilePath:String() Property
		Return Self.filePath
	End
	Method FilePath:Void(value:String) Property
		Self.filePath = value
	End

	#Rem monkeydoc
	The main class that holds all the tiled map data.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Local copyMap := gameMap.Map
	</pre>
	#End
	Method Map:Tiled_Map<I>() Property
		Return Self.map
	End
	Method Map:Void(value:Tiled_Map<I>) Property
		Self.map = value
	End
	
	#Rem monkeydoc
	If set to true, any error with the map file will stop execution. If false, then many errors with a map file will be ignored.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	If gameMap.Map.StopOnError = True
		Error
	EndIf
	</pre>
	#End
	Method StopOnError:Bool() Property
		Return Self.stopOnError
	End
	Method StopOnError:Void(value:Bool) Property
		Self.stopOnError = value
	End
	
	Private
	Method xmlReadMap:Tiled_Map<I>(xmlNode:XMLNode)
		Local map:= New Tiled_Map<I>()
		
		map.Version = xmlNode.GetAttribute("version", "1.0")
		map.Orientation = xmlNode.GetAttribute("orientation")
		map.RenderOrder = xmlNode.GetAttribute("renderorder", "right-down")
		map.Width = xmlNode.GetAttribute("width", 0)
		map.Height = xmlNode.GetAttribute("height", 0)
		map.TileWidth = xmlNode.GetAttribute("tilewidth", 0)
		map.TileHeight = xmlNode.GetAttribute("tileheight", 0)
		map.HexSideLength = xmlNode.GetAttribute("hexsidelength", 0)
		map.StaggerAxis = xmlNode.GetAttribute("staggeraxis")
		map.StaggerIndex = xmlNode.GetAttribute("staggerindex")
		map.BackgroundColor = xmlNode.GetAttribute("backgroundcolor")
		map.NextObjectID = xmlNode.GetAttribute("nextobjectid", 0)
		
		map.Properties = xmlReadProperties(xmlNode)
		map.Tileset = xmlReadTileset(xmlNode)
		map.Tileset.Sort() 'the spec requires tilesets be sorted by firstGID
		map.Layer = xmlReadLayer(xmlNode)
		map.ObjectGroup = xmlReadObjectGroup(xmlNode)
		map.ImageLayer = xmlReadImageLayer(xmlNode)
		
		Return map
	End
	
	Method xmlReadTileset:Tiled_TilesetList<I>(xmlNode:XMLNode)
		Local xmlChildren:= xmlNode.GetChildren("tileset")
		
		Local tileset:= New Tiled_TilesetList<I>
			
		For Local xmlChild:= EachIn xmlChildren
			tileset.AddLast(New Tiled_Tileset<I>())
				
			tileset.Last().FirstGID = xmlChild.GetAttribute("firstgid", 0)
			tileset.Last().Source = xmlChild.GetAttribute("source")	
			If ExtractExt(tileset.Last().Source) = "tsx"
			
				Local xmlError:= New XMLError
				
				Local tsxFile:= LoadString(ExtractDir(Self.FilePath) + "/" + tileset.Last().Source)
				
				If tsxFile = ""
					Error "Error loading file at location: " + ExtractDir(Self.FilePath) + "/" + tileset.Last().Source
				EndIf
				
				Local xml:= ParseXML(tsxFile, xmlError)
					
				If xml = Null And xmlError.error
					'error
					If Self.StopOnError = True
						Error xmlError.ToString()
					Else
						DebugLog xmlError.ToString()
					EndIf
					
					Return
				EndIf
							
				tileset.Last().Name = xml.GetAttribute("name")
				tileset.Last().TileWidth = xml.GetAttribute("tilewidth", 0)
				tileset.Last().TileHeight = xml.GetAttribute("tileheight", 0)
				tileset.Last().Spacing = xml.GetAttribute("spacing", 0)
				tileset.Last().Margin = xml.GetAttribute("margin", 0)
				tileset.Last().TileCount = xml.GetAttribute("tilecount", 0)
				tileset.Last().Columns = xml.GetAttribute("columns", 0)
								
				tileset.Last().TileOffset = Self.xmlReadTileOffset(xml)
				tileset.Last().Properties = Self.xmlReadProperties(xml)
				tileset.Last().Image = Self.xmlReadImage(xml)
				tileset.Last().TerrainTypes = Self.xmlReadTerrainTypes(xml)
				tileset.Last().Tile = Self.xmlReadTile(xml)
				
			Else
				tileset.Last().Name = xmlChild.GetAttribute("name")
				tileset.Last().TileWidth = xmlChild.GetAttribute("tilewidth", 0)
				tileset.Last().TileHeight = xmlChild.GetAttribute("tileheight", 0)
				tileset.Last().Spacing = xmlChild.GetAttribute("spacing", 0)
				tileset.Last().Margin = xmlChild.GetAttribute("margin", 0)
				tileset.Last().TileCount = xmlChild.GetAttribute("tilecount", 0)
				tileset.Last().Columns = xmlChild.GetAttribute("columns", 0)
					
				tileset.Last().TileOffset = Self.xmlReadTileOffset(xmlChild)
				tileset.Last().Properties = Self.xmlReadProperties(xmlChild)
				tileset.Last().Image = Self.xmlReadImage(xmlChild)
				tileset.Last().TerrainTypes = Self.xmlReadTerrainTypes(xmlChild)
				tileset.Last().Tile = Self.xmlReadTile(xmlChild)
			EndIf
									
		Next
		
		Return tileset
	End

	Method xmlReadTileOffset:Tiled_TileOffset(xmlNode:XMLNode)
		Local tileOffset:= New Tiled_TileOffset()
		
		Local xmlChildren:= xmlNode.GetChildren("tileoffset")
		If xmlChildren.Count > 0
			tileOffset.X = xmlChildren.First().GetAttribute("x", 0)
			tileOffset.Y = xmlChildren.First().GetAttribute("y", 0)
		EndIf
		Return tileOffset
	End
	
	Method xmlReadImage:Tiled_Image<I>(xmlNode:XMLNode)
		Local xmlChildren:= xmlNode.GetChildren("image")
		Local image:= New Tiled_Image<I>()
		
		For Local xmlChild:= EachIn xmlChildren
							
			image.Format = xmlChild.GetAttribute("format")
			image.Source = xmlChild.GetAttribute("source")
			image.Trans = xmlChild.GetAttribute("trans")
			image.Width = xmlChild.GetAttribute("width", 0)
			image.Height = xmlChild.GetAttribute("height", 0)

			image.Data = Self.xmlReadData(xmlChild)
		Next
		
		Return image
	End
	
	Method xmlReadTerrainTypes:Tiled_TerrainTypes(xmlNode:XMLNode)
		Local xmlChildren:= xmlNode.GetChildren("terraintypes")
		
		Local terraintypes:= New Tiled_TerrainTypes()
		
		For Local xmlChild:= EachIn xmlChildren
			Local xmlSubChildren:= xmlChild.GetChildren("terrain")
			For Local xmlSubChild:= EachIn xmlSubChildren
				terraintypes.Terrain.Add(xmlSubChild.GetAttribute("name"), New Tiled_Terrain)

				terraintypes.Terrain.Get(xmlSubChild.GetAttribute("name")).Tile = xmlSubChild.GetAttribute("tile", 0)
				terraintypes.Terrain.Get(xmlSubChild.GetAttribute("name")).Properties = xmlReadProperties(xmlSubChild)
			Next
		Next
		
		Return terraintypes
	End
	
	Method xmlReadTile:IntMap<Tiled_Tile<I>>(xmlNode:XMLNode)
		Local xmlChildren:= xmlNode.GetChildren("tile")
		Local tile:= New IntMap<Tiled_Tile<I>>()

		For Local xmlChild:= EachIn xmlChildren
			tile.Add(Int(xmlChild.GetAttribute("id")), New Tiled_Tile<I>())
					
			tile.Get(xmlChild.GetAttribute("id", 0)).ID = xmlChild.GetAttribute("id", 0)
			tile.Get(xmlChild.GetAttribute("id", 0)).Terrain = xmlChild.GetAttribute("terrain")
			tile.Get(xmlChild.GetAttribute("id", 0)).Probability = xmlChild.GetAttribute("probability", 1.0)			
			tile.Get(xmlChild.GetAttribute("id", 0)).Properties = Self.xmlReadProperties(xmlChild)
			tile.Get(xmlChild.GetAttribute("id", 0)).Image = Self.xmlReadImage(xmlChild)
	
		Next
		
		Return tile
	End

	Method xmlReadLayer:List<Tiled_Layer>(xmlNode:XMLNode)
		Local xmlChildren:= xmlNode.GetChildren("layer")
		Local layer:= New List<Tiled_Layer>()

		For Local xmlChild:= EachIn xmlChildren
			layer.AddLast(New Tiled_Layer)
			
			layer.Last().Name = xmlChild.GetAttribute("name")
			layer.Last().Opacity = xmlChild.GetAttribute("opacity", 1.0)
			layer.Last().Visible = xmlChild.GetAttribute("visible", 1)
			layer.Last().OffsetX = xmlChild.GetAttribute("offsetx", 0)
			layer.Last().OffsetY = xmlChild.GetAttribute("offsety", 0)

			layer.Last().Properties = Self.xmlReadProperties(xmlChild)
			layer.Last().Data = Self.xmlReadData(xmlChild)
		Next
		
		Return layer
	End
	
	Method xmlReadData:Tiled_Data(xmlNode:XMLNode)
	
		Local xmlChildren:= xmlNode.GetChildren("data")
		
		Local data:= New Tiled_Data()

		For Local xmlChild:= EachIn xmlChildren
			data.Encoding = xmlChild.GetAttribute("encoding")
			data.Compression = xmlChild.GetAttribute("compression")
			If data.Encoding = "xml" Or data.Encoding = ""
				Local tiles:= Self.xmlReadDataTiles(xmlChild)
				For Local tile:= EachIn tiles
					data.Value.AddLast(tile)
				Next
			Else
				data.RawValue = xmlChild.value
			EndIf
		Next
		
		Return data
	End
	
	Method xmlReadDataTiles:IntList(xmlNode:XMLNode)
		Local xmlChildren:= xmlNode.GetChildren("tile")
		
		Local tiles:IntList = New IntList()

		For Local xmlChild:= EachIn xmlChildren
			tiles.AddLast(xmlChild.GetAttribute("gid", 0))

		Next
		
		Return tiles
	End
	
	Method xmlReadObjectGroup:List<Tiled_ObjectGroup<I>>(xmlNode:XMLNode)
		Local xmlChildren:= xmlNode.GetChildren("objectgroup")
		Local objectGroup:= New List<Tiled_ObjectGroup<I>>()

		For Local xmlChild:= EachIn xmlChildren
			objectGroup.AddLast(New Tiled_ObjectGroup<I>)
			
			objectGroup.Last().Name = xmlChild.GetAttribute("name")
			objectGroup.Last().Color = xmlChild.GetAttribute("color")
			objectGroup.Last().Opacity = xmlChild.GetAttribute("opacity", 1.0)
			objectGroup.Last().Visible = xmlChild.GetAttribute("visible", 1)
			objectGroup.Last().OffsetY = xmlChild.GetAttribute("offsetx", 0)
			objectGroup.Last().OffsetX = xmlChild.GetAttribute("offsety", 0)
			objectGroup.Last().DrawOrder = xmlChild.GetAttribute("draworder", "topdown")

			objectGroup.Last().Properties = Self.xmlReadProperties(xmlChild)
			objectGroup.Last().Object = Self.xmlReadObject(xmlChild)
		Next
		
		Return objectGroup
	End
	
	Method xmlReadObject:List<Tiled_Object<I>>(xmlNode:XMLNode)
		Local xmlChildren:= xmlNode.GetChildren("object")
		Local object:= New List<Tiled_Object<I>>()

		For Local xmlChild:= EachIn xmlChildren
			object.AddLast(New Tiled_Object<I>)
			
			object.Last().ID = xmlChild.GetAttribute("id", 0)
			object.Last().Name = xmlChild.GetAttribute("name")
			object.Last().Type = xmlChild.GetAttribute("type")
			object.Last().X = xmlChild.GetAttribute("x", 0)
			object.Last().Y = xmlChild.GetAttribute("y", 0)
			object.Last().Width = xmlChild.GetAttribute("width", 0)
			object.Last().Height = xmlChild.GetAttribute("height", 0)
			object.Last().Rotation = xmlChild.GetAttribute("rotation", 0.0)
			object.Last().GID = xmlChild.GetAttribute("gid", 0)
			object.Last().Visible = xmlChild.GetAttribute("visible", 1)

			object.Last().Properties = Self.xmlReadProperties(xmlChild)
			object.Last().Ellipse = Self.xmlReadEllipse(xmlChild)
			object.Last().Polygon = Self.xmlReadPolygon(xmlChild)
			object.Last().Polyline = Self.xmlReadPolyline(xmlChild)
			object.Last().Image = Self.xmlReadImage(xmlChild)
		Next
		
		Return object
	End

	Method xmlReadEllipse:Tiled_Ellipse(xmlNode:XMLNode)
		Local xmlChildren:= xmlNode.GetChildren("ellipse")
		
		Local ellipse:Tiled_Ellipse = Null
		
		For Local xmlChild:= EachIn xmlChildren
			ellipse = New Tiled_Ellipse() 'make it non-null if it is an ellipse
		Next
		
		Return ellipse
	End

	Method xmlReadPolygon:Tiled_Polygon(xmlNode:XMLNode)
		Local xmlChildren:= xmlNode.GetChildren("polygon")
		
		Local polygon:= New Tiled_Polygon()
		
		For Local xmlChild:= EachIn xmlChildren
			polygon.Points = xmlChild.GetAttribute("polygon")
		Next
		
		Return polygon
	End

	Method xmlReadPolyline:Tiled_Polyline(xmlNode:XMLNode)
		Local xmlChildren:= xmlNode.GetChildren("polyline")
		
		Local polyline:= New Tiled_Polyline()
		
		For Local xmlChild:= EachIn xmlChildren
			polyline.Points = xmlChild.GetAttribute("polyline")
		Next
		
		Return polyline
	End
	
	Method xmlReadImageLayer:List<Tiled_ImageLayer<I>>(xmlNode:XMLNode)
		Local xmlChildren:= xmlNode.GetChildren("imagelayer")
		Local imageLayer:= New List<Tiled_ImageLayer<I>>()

		For Local xmlChild:= EachIn xmlChildren
			imageLayer.AddLast(New Tiled_ImageLayer<I>)

			imageLayer.Last().Name = xmlChild.GetAttribute("name")
			imageLayer.Last().OffsetX = xmlChild.GetAttribute("offsetx", 0)
			imageLayer.Last().OffsetY = xmlChild.GetAttribute("offsety", 0)
			imageLayer.Last().Opacity = xmlChild.GetAttribute("opacity", 1.0)
			imageLayer.Last().Visible = xmlChild.GetAttribute("visible", 1)

			imageLayer.Last().Properties = Self.xmlReadProperties(xmlChild)
			imageLayer.Last().Image = Self.xmlReadImage(xmlChild)
		Next
		
		Return imageLayer
	End
	
	Method xmlReadProperties:Tiled_Properties(xmlNode:XMLNode)
		Local xmlChildren:= xmlNode.GetChildren("properties")
		
		Local properties:= New Tiled_Properties()
		
		For Local xmlChild:= EachIn xmlChildren
			Local xmlSubChildren:= xmlChild.GetChildren("property")
			For Local xmlSubChild:= EachIn xmlSubChildren
				properties.Add(xmlSubChild.GetAttribute("name"), xmlSubChild.GetAttribute("value"))
				'If xmlSubChild.value <> "" Then properties.Add(xmlSubChild.GetAttribute("name"), xmlSubChild.value) 'possible future feature
			Next
		Next
		
		Return properties
	End
	
	Field map:Tiled_Map<I>
	Field filePath:String
	Field stopOnError:Bool

End 


Class Tiled_Map<I>
	Public

	#Rem monkeydoc
	Stores the version of the map file. Normally the value is "1.0".
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Version()
	</pre>
	#End
	Method Version:String() Property
		Return Self.version
	End
	Method Version:Void(value:String) Property
		Self.version = value
	End
	
	#Rem monkeydoc
	Stores the orientation of the map. Tiled supports "orthogonal", "isometric", "staggered" and "hexagonal."
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Orientation()
	</pre>
	#End
	Method Orientation:String() Property
		Return Self.orientation
	End
	Method Orientation:Void(value:String) Property
		Self.orientation = value
	End
	
	#Rem monkeydoc
	Stores the render order of the map. The order in which tiles on tile layers are rendered. Valid values are right-down (the default), right-up, left-down and left-up. In all cases, the map is drawn row-by-row.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.RenderOrder()
	</pre>
	#End
	Method RenderOrder:String() Property
		Return Self.renderOrder
	End
	Method RenderOrder:Void(value:String) Property
		Self.renderOrder = value
	End
	
	#Rem monkeydoc
	Stores the width of the map in tiles.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Width()
	</pre>
	#End
	Method Width:Int() Property
		Return Self.width
	End
	Method Width:Void(value:Int) Property
		Self.width = value
	End
	
	#Rem monkeydoc
	Stores the height of the map in tiles.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Height()
	</pre>
	#End

	Method Height:Int() Property
		Return Self.height
	End
	Method Height:Void(value:Int) Property
		Self.height = value
	End
	
	#Rem monkeydoc
	Stores the width of a tile in pixels.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.TileWidth()
	</pre>
	#End
	Method TileWidth:Int() Property
		Return Self.tileWidth
	End
	Method TileWidth:Void(value:Int) Property
		Self.tileWidth = value
	End
	
	#Rem monkeydoc
	Stores the height of a tile in pixels.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.TileHeight()
	</pre>
	#End
	Method TileHeight:Int() Property
		Return Self.tileHeight
	End
	Method TileHeight:Void(value:Int) Property
		Self.tileHeight = value
	End

	#Rem monkeydoc
	Stores the hex side length of the hexagonal tiles in a hexagonal or staggered maps, in pixels.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.HexSideLength()
	</pre>
	#End
	Method HexSideLength:Int() Property
		Return Self.hexsidelength
	End
	Method HexSideLength:Void(value:Int) Property
		Self.hexsidelength = value
	End
	
	#Rem monkeydoc
	Stores the staggered axis setting for a map. For staggered and hexagonal maps, determines which axis ("x" or "y") is staggered.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.StaggerAxis()
	</pre>
	#End
	Method StaggerAxis:String() Property
		Return Self.staggerAxis
	End
	Method StaggerAxis:Void(value:String) Property
		Self.staggerAxis = value
	End

	#Rem monkeydoc
	For staggered and hexagonal maps, stores whether the "even" or "odd" indexes along the staggered axis are shifted.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.StaggerIndex()
	</pre>
	#End
	Method StaggerIndex:String() Property
		Return Self.staggerIndex
	End
	Method StaggerIndex:Void(value:String) Property
		Self.staggerIndex = value
	End

	#Rem monkeydoc
	Stores the background color of the map in the form of "#RRGGBBAA". May or may not include the alpha value.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.BackgroundColor()
	</pre>
	#End
	Method BackgroundColor:String() Property
		Return Self.backgroundColor
	End
	Method BackgroundColor:Void(value:String) Property
		Self.backgroundColor = CleanColor(value)
	End
		
	#Rem monkeydoc
	Stores the next available ID for new objects. This number is stored to prevent reuse of the same ID after objects have been removed.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.NextObjectID()
	</pre>
	#End
	Method NextObjectID:Int() Property
		Return Self.nextObjectID
	End
	Method NextObjectID:Void(value:Int) Property
		Self.nextObjectID = value
	End

	#Rem monkeydoc
	Stores any number of custom properties associated with a map.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Properties.Count()
	Print gameMap.Map.Properties.Get("hello")
	</pre>
	#End
	Method Properties:Tiled_Properties() Property
		Return Self.properties
	End
	Method Properties:Void(value:Tiled_Properties) Property
		Self.properties = value
	End

	#Rem monkeydoc
	Stores any number of tilesets associated with a map. These should always be sorted by FirstGID per the tiled spec if you add or remove any tilesets. The renderer can rely on this behavior.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.Count()
	Print gameMap.Map.Tileset.First().Name()
	gameMap.Map.Tileset.Sort()
	</pre>
	#End
	Method Tileset:Tiled_TilesetList<I>() Property
		Return Self.tileset
	End
	Method Tileset:Void(value:Tiled_TilesetList<I>) Property
		Self.tileset = value
	End
	
	#Rem monkeydoc
	Stores any number of layers associated with a map.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Layer.Count()
	Print gameMap.Map.Layer.First().Name()
	</pre>
	#End
	Method Layer:List<Tiled_Layer>() Property
		Return Self.layer
	End
	Method Layer:Void(value:List<Tiled_Layer>) Property
		Self.layer = value
	End
	
	#Rem monkeydoc
	Stores any number of object groups associated with a map.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.ObjectGroup.Count()
	Print gameMap.Map.ObjectGroup.First().Name()
	</pre>
	#End
	Method ObjectGroup:List<Tiled_ObjectGroup<I>>() Property
		Return Self.objectGroup
	End
	Method ObjectGroup:Void(value:List<Tiled_ObjectGroup<I>>) Property
		Self.objectGroup = value
	End
	
	#Rem monkeydoc
	Stores any number of image layers associated with a map.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.ImageLayer.Count()
	Print gameMap.Map.ImageLayer.First().Name()
	</pre>
	#End
	Method ImageLayer:List<Tiled_ImageLayer<I>>() Property
		Return Self.imageLayer
	End
	Method ImageLayer:Void(value:List<Tiled_ImageLayer<I>>) Property
		Self.imageLayer = value
	End

	Private
	Field version:String
	Field orientation:String
	Field renderOrder:String
	Field width:Int, height:Int
	Field tileWidth:Int, tileHeight:Int
	Field hexsidelength:Int
	Field staggerAxis:String
	Field staggerIndex:String
	Field backgroundColor:String
	Field nextObjectID:Int
	
	Field properties:Tiled_Properties
	Field tileset:Tiled_TilesetList<I> = New Tiled_TilesetList<I>
	Field layer:List<Tiled_Layer> = New List<Tiled_Layer>
	Field objectGroup:List<Tiled_ObjectGroup<I>> = New List<Tiled_ObjectGroup<I>>
	Field imageLayer:List<Tiled_ImageLayer<I>> = New List<Tiled_ImageLayer<I>>
End

Class Tiled_Tileset<I>
	Public
	
	#Rem monkeydoc
	Stores the first global tile ID of the tileset (this global ID maps to the first tile in this tileset).
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().FirstGID
	</pre>
	#End
	Method FirstGID:Int() Property
		Return Self.firstGID
	End
	Method FirstGID:Void(value:Int) Property
		Self.firstGID = value
	End
	
	#Rem monkeydoc
	If the tileset is stored in an external TSX (Tile Set XML) file, this property refers to that file.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().Source
	</pre>
	#End
	Method Source:String() Property
		Return Self.source
	End
	Method Source:Void(value:String) Property
		Self.source = value
	End
	
	#Rem monkeydoc
	Stores the name of the tileset.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().Name
	</pre>
	#End
	Method Name:String() Property
		Return Self.name
	End
	Method Name:Void(value:String) Property
		Self.name = value
	End
	
	#Rem monkeydoc
	Stores the (maximum) width of the tiles in this tileset. When rendering an entire map,
	you should instead use the TileWidth property from the Tiled_Map class. When drawing a
	specific tile in a tileset, you can use this property to find the width. If the tile is
	larger than the map's tile width, then the excess should be rendered over the right side.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().TileWidth
	</pre>
	#End
	Method TileWidth:Int() Property
		Return Self.tileWidth
	End
	Method TileWidth:Void(value:Int) Property
		Self.tileWidth = value
	End
	
	#Rem monkeydoc
	Stores the (maximum) height of the tiles in this tileset. When rendering an entire map,
	you should instead use the TileHeight property from the Tiled_Map class. When drawing a
	specific tile in a tileset, you can use this property to find the height. If the tile is
	larger than the map's tile height, then the excess should be rendered over the top side.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().TileHeight
	</pre>
	#End
	Method TileHeight:Int() Property
		Return Self.tileHeight
	End
	Method TileHeight:Void(value:Int) Property
		Self.tileHeight = value
	End

	#Rem monkeydoc
	Stores the spacing in pixels between the tiles in this tileset (applies to the tileset image).
	The spacing is only between the tiles but not on the outside edge of the image.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().Spacing
	</pre>
	#End
	Method Spacing:Int() Property
		Return Self.spacing
	End
	Method Spacing:Void(value:Int) Property
		Self.spacing = value
	End
	
	#Rem monkeydoc
	Stores the margin around the tiles in this tileset (applies to the tileset image).
	Specifically, this is only along the outside border of the image.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().Margin
	</pre>
	#End
	Method Margin:Int() Property
		Return Self.margin
	End
	Method Margin:Void(value:Int) Property
		Self.margin = value
	End
	
	#Rem monkeydoc
	Stores the number of tiles in this tileset. Some maps may not have this set, in which case you must
	calculate it manually. See example1.monkey for a way to calculate this value.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().TileCount
	</pre>
	#End
	Method TileCount:Int() Property
		Return Self.tileCount
	End
	Method TileCount:Void(value:Int) Property
		Self.tileCount = value
	End
	
	#Rem monkeydoc
	Stores the number of tile columns in the tileset. For image collection tilesets it is editable and is used when
	displaying the tileset in Tiled. Some maps may not have this set.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().Columns
	</pre>
	#End	
	Method Columns:Int() Property
		Return Self.columns
	End
	Method Columns:Void(value:Int) Property
		Self.columns = value
	End
	
	#Rem monkeydoc
	Stores the TileOffset object, which is used to specify an offset in pixels, to be applied when drawing a tile from
	the related tileset. When not present, no offset is applied.
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	If gameMap.Map.Tileset.First().TileOffset <> Null
		Print "There is a tile offset."
	EndIf
	</pre>
	#End	
	Method TileOffset:Tiled_TileOffset() Property
		Return Self.tileOffset
	End
	Method TileOffset:Void(value:Tiled_TileOffset) Property
		Self.tileOffset = value
	End

	#Rem monkeydoc
	Stores the Properties object, which wraps any number of custom properties. This is basically just a
	StringMap<String> object.

	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	gameMap.Map.Tileset.First().Properties.Add("foo", "bar")
	Print gameMap.Map.Tileset.First().Properties.Get("foo")
	</pre>
	#End
	Method Properties:Tiled_Properties() Property
		Return Self.properties
	End
	Method Properties:Void(value:Tiled_Properties) Property
		Self.properties = value
	End
	
	#Rem monkeydoc
	Stores an Tiled_Image object which describes the tileset image.

	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	If gameMap.Map.Tileset.First().Image = Null
		Print "There's probably an error in your map file. It's missing the <Image> element."	
	Endif
	</pre>
	#End	
	Method Image:Tiled_Image<I>() Property
		Return Self.image
	End
	Method Image:Void(value:Tiled_Image<I>) Property
		Self.image = value
	End
	
	#Rem monkeydoc
	Stores an object that holds a list of terrain types, which is looked up from the terrain property of a Tiled_Tile object.
	This is more or less just a feature for editing maps in Tiled. You can look at the following for more information:
	http://doc.mapeditor.org/reference/tmx-map-format/#terraintypes
	

	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Local tileToCheck:Int = 1
	Local TerrainNameToCheck:String = gameMap.Map.Tileset.First().Tile.Get(tileToCheck).Terrain
	'Prints the tile number to use for the terrain
	Print gameMap.Map.Tileset.First().TerrainTypes.Terrain.Get(TerrainNameToCheck).Tile
	</pre>
	#End	
	Method TerrainTypes:Tiled_TerrainTypes() Property
		Return Self.terrainTypes
	End
	Method TerrainTypes:Void(value:Tiled_TerrainTypes) Property
		Self.terrainTypes = value
	End

	#Rem monkeydoc
	Stores an Tiled_Tile object which holds tile information for tiles in the tileset:
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().Tile.Count()
	</pre>
	#End	
	Method Tile:IntMap<Tiled_Tile<I>>() Property
		Return Self.tile
	End
	Method Tile:Void(value:IntMap<Tiled_Tile<I>>) Property
		Self.tile = value
	End

	Private
	Field firstGID:Int
	Field source:String
	Field name:String
	Field tileWidth:Int
	Field tileHeight:Int
	Field spacing:Int
	Field margin:Int
	Field tileCount:Int
	Field columns:Int
	
	Field tileOffset:Tiled_TileOffset = New Tiled_TileOffset
	Field properties:Tiled_Properties = New Tiled_Properties
	Field image:Tiled_Image<I> = New Tiled_Image<I>
	Field terrainTypes:Tiled_TerrainTypes = New Tiled_TerrainTypes
	Field tile:IntMap<Tiled_Tile<I>> = New IntMap<Tiled_Tile<I>>

End

Class Tiled_TileOffset
	Public
	
	#Rem monkeydoc
	Stores the X offset for the object in pixels:
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().TileOffset.X
	</pre>
	#End	
	Method X:Int() Property
		Return Self.x
	End
	Method X:Void(value:Int) Property
		Self.x = value
	End

	#Rem monkeydoc
	Stores the Y offset for the object in pixels:
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().TileOffset.Y
	</pre>
	#End	
	Method Y:Int() Property
		Return Self.y
	End
	Method Y:Void(value:Int) Property
		Self.y = value
	End

	Private
	Field x:Int, y:Int
End

Class Tiled_Image<I>
	Public

	#Rem monkeydoc
	Stores the image format for embedded images in a map file, in combination with a data child element.
	Valid values are file extensions like png, gif, jpg, bmp, etc.:
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().Image.Format
	Print gameMap.Map.Tileset.First().Tile.Get(1).Image.Format
	Print gameMap.Map.ObjectGroup.First().Object.First().Image.Format
	Print gameMap.Map.ImageLayer.First().Image.Format
	</pre>
	#End
	Method Format:String() Property
		Return Self.format
	End
	Method Format:Void(value:String) Property
		Self.format = value
	End
	
	#Rem monkeydoc
	Stores the path to the tileset image file:
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().Image.Source
	Print gameMap.Map.Tileset.First().Tile.Get(1).Image.Source
	Print gameMap.Map.ObjectGroup.First().Object.First().Image.Source
	Print gameMap.Map.ImageLayer.First().Image.Source
	</pre>
	#End
	Method Source:String() Property
		Return Self.source
	End
	Method Source:Void(value:String) Property
		Self.source = value
	End

	#Rem monkeydoc
	Stores the actual image object. This is not a tiled feature, but a feature of this library.
	You can use this to easily load your images into memory. You can set the class that is used here
	when you instantiate you map. In the example, <Image> specifies that this will hold an object
	of type "Image". You will have to load the image yourself. See example1.monkey for a detailed
	example of loading resources and displaying maps:
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	DrawImage(gameMap.Map.Tileset.First().Image.ImageResource, 0,0)
	DrawImage(gameMap.Map.Tileset.First().Tile.Get(1).Image.ImageResource, 0,0)
	DrawImage(gameMap.Map.ObjectGroup.First().Object.First().Image.ImageResource, 0,0)
	DrawImage(gameMap.Map.ImageLayer.First().Image.ImageResource, 0,0)
	</pre>
	#End
	Method ImageResource:I() Property
		Return Self.imageResource
	End
	Method ImageResource:Void(value:I) Property
		Self.imageResource = value
	End
	
	#Rem monkeydoc
	Stores a specific color that is treated as transparent (example value: "#FF00FF" for magenta).
	Up until Tiled 0.12, this value is written out without a # but this is planned to change:
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().Image.Trans
	Print gameMap.Map.Tileset.First().Tile.Get(1).Image.Trans
	Print gameMap.Map.ObjectGroup.First().Object.First().Image.Trans
	Print gameMap.Map.ImageLayer.First().Image.Trans
	</pre>
	#End
	Method Trans:String() Property
		Return Self.trans
	End
	Method Trans:Void(value:String) Property
		Self.trans = CleanColor(value)
	End
	
	#Rem monkeydoc
	Stores the image width in pixels (optional, used for tile index correction in Tiled when the image changes.):
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().Image.Width
	Print gameMap.Map.Tileset.First().Tile.Get(1).Image.Width
	Print gameMap.Map.ObjectGroup.First().Object.First().Image.Width
	Print gameMap.Map.ImageLayer.First().Image.Width
	</pre>
	#End
	Method Width:Int() Property
		Return Self.width
	End
	Method Width:Void(value:Int) Property
		Self.width = value
	End
	
	#Rem monkeydoc
	Stores the image height in pixels (optional, used for tile index correction in Tiled when the image changes.):
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Print gameMap.Map.Tileset.First().Image.Height
	Print gameMap.Map.Tileset.First().Tile.Get(1).Image.Height
	Print gameMap.Map.ObjectGroup.First().Object.First().Image.Height
	Print gameMap.Map.ImageLayer.First().Image.Height
	</pre>
	#End
	Method Height:Int() Property
		Return Self.height
	End
	Method Height:Void(value:Int) Property
		Self.height = value
	End

	#Rem monkeydoc
	Stores an object of type Tiled_Data. Used for storing map or image data:
	
	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	If gameMap.Map.Layer.First().Data <> Null
		Print "Layer data exists and is what holds the map tile data, but it's parsed by this library for your convenience."
	EndIf
	'prints the encoding of the data stored
	Print gameMap.Map.Tileset.First().Image.Data.Encoding
	Print gameMap.Map.Tileset.First().Tile.Get(1).Image.Data.Encoding
	Print gameMap.Map.ObjectGroup.First().Object.First().Image.Data.Encoding
	Print gameMap.Map.ImageLayer.First().Image.Data.Encoding

	</pre>
	#End
	Method Data:Tiled_Data() Property
		Return Self.data
	End
	Method Data:Void(value:Tiled_Data) Property
		Self.data = value
	End
	
	Private
	Field format:String
	Field imageResource:I
	Field source:String
	Field trans:String
	Field width:Int
	Field height:Int
	
	Field data:Tiled_Data
End

Class Tiled_TerrainTypes
	Public
	
	#Rem monkeydoc
	Stores a stringmap that holds a list of terrain types, which is looked up from the terrain property of a Tiled_Tile object.
	This is more or less just a feature for editing maps in Tiled. You can look at the following for more information:
	http://doc.mapeditor.org/reference/tmx-map-format/#terraintypes
	

	Example:
	<pre>
	Local gameMap := New Tiled<Image>("tiledmap.tmx")
	Local tileToCheck:Int = 1
	Local TerrainNameToCheck:String = gameMap.Map.Tileset.First().Tile.Get(tileToCheck).Terrain
	'Prints the tile number to use for the terrain
	Print gameMap.Map.Tileset.First().TerrainTypes.Terrain.Get(TerrainNameToCheck).Tile
	</pre>
	#End	
	Method Terrain:StringMap<Tiled_Terrain>() Property
		Return Self.terrain
	End
	Method Terrain:Void(value:StringMap<Tiled_Terrain>) Property
		Self.terrain = value
	End
	
	Private
	Field terrain:StringMap<Tiled_Terrain> = New StringMap<Tiled_Terrain>
End

Class Tiled_Terrain
	Public

	Method Tile:Int() Property
		Return Self.tile
	End
	Method Tile:Void(value:Int) Property
		Self.tile = value
	End

	Method Properties:Tiled_Properties() Property
		Return Self.properties
	End
	Method Properties:Void(value:Tiled_Properties) Property
		Self.properties = value
	End
	
	Private
	Field tile:Int
	
	Field properties:Tiled_Properties
End

Class Tiled_Tile<I>
	Public
	
	Method ID:Int() Property
		Return Self.id
	End
	Method ID:Void(value:Int) Property
		Self.id = value
	End
	
	Method Terrain:String() Property
		Return Self.terrain
	End
	Method Terrain:Void(value:String) Property
		Self.terrain = value
	End
	
	Method Probability:Float() Property
		Return Self.probability
	End
	Method Probability:Void(value:Float) Property
		Self.probability = value
	End

	Method Properties:Tiled_Properties() Property
		Return Self.properties
	End
	Method Properties:Void(value:Tiled_Properties) Property
		Self.properties = value
	End
	
	Method Image:Tiled_Image<I>() Property
		Return Self.image
	End
	Method Image:Void(value:Tiled_Image<I>) Property
		Self.image = value
	End
	
	Method ObjectGroup:Tiled_ObjectGroup() Property
		Return Self.objectGroup
	End
	Method ObjectGroup:Void(value:Tiled_ObjectGroup) Property
		Self.objectGroup = value
	End
	
	Method Animation:Tiled_Animation() Property
		Return Self.animation
	End
	Method Animation:Void(value:Tiled_Animation) Property
		Self.animation = value
	End
	
	Private
	Field id:Int
	Field terrain:String
	Field probability:Float
	
	Field properties:Tiled_Properties
	Field image:Tiled_Image<I> = New Tiled_Image<I>
	Field objectGroup:Tiled_ObjectGroup
	Field animation:Tiled_Animation
	
End

Class Tiled_Animation
	Public
	
	Method Frame:List<Tiled_Frame>() Property
		Return Self.frame
	End
	Method Frame:Void(value:List<Tiled_Frame>) Property
		Self.frame = value
	End
	
	Private
	Field frame:List<Tiled_Frame> = New List<Tiled_Frame>
End

Class Tiled_Frame
	Public

	Method TileID:Int() Property
		Return Self.tileID
	End
	Method TileID:Void(value:Int) Property
		Self.tileID = value
	End

	Method Duration:Int() Property
		Return Self.duration
	End
	Method Duration:Void(value:Int) Property
		Self.duration = value
	End

	Private
	Field tileID:Int
	Field duration:Int
End


Class Tiled_Layer
	Public

	Method Name:String() Property
		Return Self.name
	End
	Method Name:Void(value:String) Property
		Self.name = value
	End

	Method Opacity:Float() Property
		Return Self.opacity
	End
	Method Opacity:Void(value:Float) Property
		Self.opacity = value
	End
	
	Method Visible:Int() Property
		Return Self.visible
	End
	Method Visible:Void(value:Int) Property
		Self.visible = value
	End

	Method OffsetX:Int() Property
		Return Self.offsetX
	End
	Method OffsetX:Void(value:Int) Property
		Self.offsetX = value
	End
		
	Method OffsetY:Int() Property
		Return Self.offsetY
	End
	Method OffsetY:Void(value:Int) Property
		Self.offsetY = value
	End
		
	Method Properties:Tiled_Properties() Property
		Return Self.properties
	End
	Method Properties:Void(value:Tiled_Properties) Property
		Self.properties = value
	End
	
	Method Data:Tiled_Data() Property
		Return Self.data
	End
	Method Data:Void(value:Tiled_Data) Property
		Self.data = value
	End
	
	Private
	Field name:String
	Field opacity:Float
	Field visible:Int
	Field offsetX:Int
	Field offsetY:Int
	
	Field properties:Tiled_Properties
	Field data:Tiled_Data
End

Class Tiled_Data
	Public
	
	Method Encoding:String() Property
		Return Self.encoding
	End
	Method Encoding:Void(value:String) Property
		Self.encoding = value
	End
	
	Method Compression:String() Property
		Return Self.compression
	End
	Method Compression:Void(value:String) Property
		Self.compression = value
	End
	
	Method Value:IntList() Property
		Return Self.value
	End
	Method Value:Void(value:IntList) Property
		Self.value = value
		Self.rawValue = Self.ComposeRawValue()
	End
	Method Value:Int(x:Int, y:Int, width:Int, height:Int)
		If x < 0 Or y < 0 Or x >= width Or y >= height
			Error("Map coordinate out of range")
		EndIf
		Return Self.value.ToArray[y * width + x]
	End

	Private
	
	Method RawValue:String() Property
		Return Self.rawValue
	End
	Method RawValue:Void(value:String) Property
		Self.rawValue = value
		Self.value = Self.ParseRawValue()
	End
	
	Method ComposeRawValue:String()
		Local value:String = ""
		
		Select Self.Encoding
			Case "csv"
				For Local i:= EachIn Self.Value
					value = value + i + ","
				Next
				value = value[ .. value.Length - 1] 'remove trailing comma
					
			Case "base64"
				value = Base64Encode(Self.Value.ToArray())

			Default 'xml or empty string
				'this is handled during the output of xml
			
		End Select

		Return value
	End
	
	Method ParseRawValue:IntList()
		Local value:IntList = New IntList()
	
		Select Self.Compression
			Case "gzip"
				Error "Map compression is not supported."
			Case "zlib"
				Error "Map compression is not supported."
			Default
				
		End
				
		'decode and populate Tile
		Select Self.Encoding
			Case "csv"
				Local rawValues:= Self.rawValue.Split(",")
					
				For Local rawValuesItem:= EachIn rawValues
					value.AddLast(Int(rawValuesItem))
				Next
			Case "base64"
				Local rawValues:= Base64Decode(Self.rawValue)

								
				'If rawdata.Length Mod 4 = 0
				For Local i:= 0 To rawValues.Length Step 4
					If i + 3 < rawValues.Length
						'add 4 bytes at a time in little endian format to determine a tile
						Local rawValuesItem:= rawValues[i] + (rawValues[i + 1] Shl 8) + (rawValues[i + 2] Shl 16) + (rawValues[i + 3] Shl 24)
						value.AddLast(rawValuesItem)
						'get remaining data before the padding, tiled does not seem to calculate padding properly or else I have a bug somewhere
					ElseIf i + 2 < rawValues.Length
						Local rawValuesItem:= rawValues[i] + (rawValues[i + 1] Shl 8) + (rawValues[i + 2] Shl 16)
						value.AddLast(rawValuesItem)
					ElseIf i + 1 < rawValues.Length
						Local rawValuesItem:= rawValues[i] + (rawValues[i + 1] Shl 8)
						value.AddLast(rawValuesItem)
					ElseIf i < rawValues.Length
						Local rawValuesItem:= rawValues[i]
						value.AddLast(rawValuesItem)
					EndIf
				Next
			Case "xml"
				'this is handled in the xmlReadData method in Tiled class
			
		End Select
		
		Return value
	End
	
	Field encoding:String
	Field compression:String
		
	Field rawValue:String
	Field value:IntList = New IntList()
		
	Field base64:StringMap<Int> = New StringMap<Int>
End

Class Tiled_ObjectGroup<I>
	Public
	
	Method Name:String() Property
		Return Self.name
	End
	Method Name:Void(value:String) Property
		Self.name = value
	End
	
	Method Color:String() Property
		Return Self.color
	End
	Method Color:Void(value:String) Property
		Self.color = CleanColor(value)
	End

	Method Opacity:Float() Property
		Return Self.opacity
	End
	Method Opacity:Void(value:Float) Property
		Self.opacity = value
	End
	
	Method Visible:Int() Property
		Return Self.visible
	End
	Method Visible:Void(value:Int) Property
		Self.visible = value
	End
	
	Method OffsetX:Int() Property
		Return Self.offsetX
	End
	Method OffsetX:Void(value:Int) Property
		Self.offsetX = value
	End
		
	Method OffsetY:Int() Property
		Return Self.offsetY
	End
	Method OffsetY:Void(value:Int) Property
		Self.offsetY = value
	End
			
	Method DrawOrder:String() Property
		Return Self.drawOrder
	End
	Method DrawOrder:Void(value:String) Property
		Self.drawOrder = value
	End
	
	Method Properties:Tiled_Properties() Property
		Return Self.properties
	End
	Method Properties:Void(value:Tiled_Properties) Property
		Self.properties = value
	End
	
	Method Object:List<Tiled_Object<I>>() Property
		Return Self.objects
	End
	Method Object:Void(value:List<Tiled_Object<I>>) Property
		Self.objects = value
	End

	Private
	Field name:String
	Field color:String
	Field opacity:Float
	Field visible:Int
	Field offsetX:Int
	Field offsetY:Int
	Field drawOrder:String
	
	Field properties:Tiled_Properties
	Field objects:List<Tiled_Object<I>> = New List<Tiled_Object<I>>
	
End

Class Tiled_Object<I>
	Public
	
	Method ID:Int() Property
		Return Self.id
	End
	Method ID:Void(value:Int) Property
		Self.id = value
	End
	
	Method Name:String() Property
		Return Self.name
	End
	Method Name:Void(value:String) Property
		Self.name = value
	End

	Method Type:String() Property
		Return Self.type
	End
	Method Type:Void(value:String) Property
		Self.type = value
	End
	
	Method X:Int() Property
		Return Self.x
	End
	Method X:Void(value:Int) Property
		Self.x = value
	End

	Method Y:Int() Property
		Return Self.y
	End
	Method Y:Void(value:Int) Property
		Self.y = value
	End
	
	Method Width:Int() Property
		Return Self.width
	End
	Method Width:Void(value:Int) Property
		Self.width = value
	End
	
	Method Height:Int() Property
		Return Self.height
	End
	Method Height:Void(value:Int) Property
		Self.height = value
	End
	
	Method Rotation:Int() Property
		Return Self.rotation
	End
	Method Rotation:Void(value:Int) Property
		Self.rotation = value
	End
	
	Method GID:Int() Property
		Return Self.gid
	End
	Method GID:Void(value:Int) Property
		Self.gid = value
	End
		
	Method Visible:Int() Property
		Return Self.visible
	End
	Method Visible:Void(value:Int) Property
		Self.visible = value
	End
	
	Method Properties:Tiled_Properties() Property
		Return Self.properties
	End
	Method Properties:Void(value:Tiled_Properties) Property
		Self.properties = value
	End
	
	Method Ellipse:Tiled_Ellipse() Property
		Return Self.ellipse
	End
	Method Ellipse:Void(value:Tiled_Ellipse) Property
		Self.ellipse = value
	End

	Method Polygon:Tiled_Polygon() Property
		Return Self.polygon
	End
	Method Polygon:Void(value:Tiled_Polygon) Property
		Self.polygon = value
	End

	Method Polyline:Tiled_Polyline() Property
		Return Self.polyline
	End
	Method Polyline:Void(value:Tiled_Polyline) Property
		Self.polyline = value
	End

	Method Image:Tiled_Image<I>() Property
		Return Self.image
	End
	Method Image:Void(value:Tiled_Image<I>) Property
		Self.image = value
	End

	Private
	Field id:Int
	Field name:String
	Field type:String
	Field x:Int
	Field y:Int
	Field width:Int
	Field height:Int
	Field rotation:Int
	Field gid:Int
	Field visible:Int
	
	Field properties:Tiled_Properties
	Field ellipse:Tiled_Ellipse
	Field polygon:Tiled_Polygon
	Field polyline:Tiled_Polyline
	Field image:Tiled_Image<I>
End

Class Tiled_Ellipse
	Public
	
End
Class Tiled_Polygon
	Public
	
	Method Points:String() Property
		Return Self.points
	End
	Method Points:Void(value:String) Property
		Self.points = value
	End
	
	Private
	Field points:String
	
End
Class Tiled_Polyline
	Public
	
	Method Points:String() Property
		Return Self.points
	End
	Method Points:Void(value:String) Property
		Self.points = value
	End

	Private
	Field points:String
End



Class Tiled_ImageLayer<I>
	Public
	
	Method Name:String() Property
		Return Self.name
	End
	Method Name:Void(value:String) Property
		Self.name = value
	End

	Method OffsetX:Int() Property
		Return Self.offsetX
	End
	Method OffsetX:Void(value:Int) Property
		Self.offsetX = value
	End
		
	Method OffsetY:Int() Property
		Return Self.offsetY
	End
	Method OffsetY:Void(value:Int) Property
		Self.offsetY = value
	End
	
	Method Opacity:Float() Property
		Return Self.opacity
	End
	Method Opacity:Void(value:Float) Property
		Self.opacity = value
	End
	
	Method Visible:Int() Property
		Return Self.visible
	End
	Method Visible:Void(value:Int) Property
		Self.visible = value
	End
	
	Method Properties:Tiled_Properties() Property
		Return Self.properties
	End
	Method Properties:Void(value:Tiled_Properties) Property
		Self.properties = value
	End
	
	Method Image:Tiled_Image<I>() Property
		Return Self.image
	End
	Method Image:Void(value:Tiled_Image<I>) Property
		Self.image = value
	End

	Private
	Field name:String
	Field offsetX:Int
	Field offsetY:Int
	Field opacity:Float
	Field visible:Int
	
	Field properties:Tiled_Properties
	Field image:Tiled_Image<I>
	
End

Class Tiled_Properties Extends StringMap<String>
	Public
	
End

Class Tiled_TilesetList<I> Extends List<Tiled_Tileset<I>>
	Public
	
	'tilesets must be sorted by FirstGID and so this method should be called any time tilesets are added or removed
	Method Compare:Int(lhs:Tiled_Tileset<I>, rhs:Tiled_Tileset<I>)
		Return lhs.FirstGID - rhs.FirstGID
	End
End

Function CleanColor:String(value:String)
	'clean up the string to make it standard
	'This will guarantee the string is either blank,
	'or in the form of #AARRGGBB and in uppercase
	'and with all values being between 00 and FF
	If value <> ""
		Local sanitizeValue:String = value
		If sanitizeValue.StartsWith("#") = True
			sanitizeValue = sanitizeValue[1 ..]
		EndIf
		If sanitizeValue.Length <= 6 'earlier versions of tiled did not include an alpha value
			'fill in the alpha value
			sanitizeValue = "FF" + sanitizeValue
		EndIf
		While sanitizeValue.Length < 8
			sanitizeValue += "0"
		Wend
		sanitizeValue = sanitizeValue[0 .. 8].ToUpper()
		
		value = "#"
		For Local i:= EachIn sanitizeValue.ToChars()
			If (i >= 48 And i <= 57) Or (i >= 65 And i <= 70)
				value += String.FromChar(i)
			Else
				value += "0"
			EndIf
		Next
	EndIf
	Return value
End