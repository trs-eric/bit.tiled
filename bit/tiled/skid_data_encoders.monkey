' monkey data encoders by simon armstrong

' functions to aid packing and unpacking integer array into packed ascii strings

' to pack data into string
' pack$=Base64Encode(UTF8Encode(RunLength32Encode(data[])))

' to unpack string into data:
' data[]=RunLength32Decode(UTFDecode(Base64Decode(pack$)))

' RunLength32Encode
' RunLength32Decode
' UTF8Encode
' UTF8Decode
' Base64Encode
' Base64Decode

' runlength functions

Function RunLength32Encode:Int[](data:Int[])
	Local encode[]
	Local count
	Local seq
	Local rept
	Local prev
	encode=New Int[data.Length+2]
	prev=data[0]-1
	seq=-1
	For Local i=0 To data.Length-1
		Local d=data[i]
		If d=prev
			rept+=1
			If rept=3 And seq>0
				encode[count-seq-3]=seq
				count+=1
				seq=0
			Endif
		Else
			prev=d
			If rept<3
				seq+=1+rept
			Else
				encode[count-3]=-rept-1
				count-=1
			Endif
			rept=0
		Endif
		If rept<3
			count+=1
			encode[count]=d
		Endif
	Next
	If rept>2
		encode[count-3]=-rept-1
		count-=2
	Else
		seq+=rept+1
	Endif
	If seq>0
		encode[count-seq]=seq
	Endif
	Return encode.Resize(count+1)
End

Function RunLength32Decode:Int[](data:Int[])
	Local decode[]
	Local count
	Local i
	While i<data.Length
		Local d=data[i]
		If d<0
			count-=d
			i=i+2
		Else
			count+=d
			i=i+1+d
		Endif
	Wend
	decode=New Int[count]
	i=0
	count=0
	While i<data.Length
		Local d=data[i]
		i+=1
		If d<0
			Local v=data[i]
			i=i+1
			While d
				decode[count]=v
				count+=1
				d+=1
			Wend
		Else
			While d
				decode[count]=data[i]
				i+=1
				count+=1
				d-=1
			Wend
		Endif
	Wend
	Return decode
End

' utf8 functions

Function UTF8Encode:Int[](data:Int[])
	Local bytes[]
	Local count
	
	bytes=New Int[data.Length*6]
	For Local d=Eachin data
		If d<0 Or d>=$4000000
			bytes[count]=$FC|((d Shr 30)&$3)
			bytes[count+1]=$80|((d Shr 24)&$3F)
			bytes[count+2]=$80|((d Shr 18)&$3F)
			bytes[count+3]=$80|((d Shr 12)&$3F)
			bytes[count+4]=$80|((d Shr 6)&$3F)
			bytes[count+5]=$80|(d&$3F)
			count+=6
			Continue
		Endif
		If d<$80
			bytes[count]=d
			count+=1
			Continue
		Endif
		If d<$800
			bytes[count]=$c0|((d Shr 6)&$1F)
			bytes[count+1]=$80|(d&$3F)
			count+=2
			Continue
		Endif
		If d<$10000
			bytes[count]=$E0|((d Shr 12)&$F)
			bytes[count+1]=$80|((d Shr 6)&$3F)
			bytes[count+2]=$80|(d&$3F)
			count+=3
			Continue
		Endif
		If d<$200000
			bytes[count]=$F0|((d Shr 18)&$7)
			bytes[count+1]=$80|((d Shr 12)&$3F)
			bytes[count+2]=$80|((d Shr 6)&$3F)
			bytes[count+3]=$80|(d&$3F)
			count+=4
			Continue
		Endif
		If d<$4000000
			bytes[count]=$F8|((d Shr 24)&$3)
			bytes[count+1]=$80|((d Shr 18)&$3F)
			bytes[count+2]=$80|((d Shr 12)&$3F)
			bytes[count+3]=$80|((d Shr 6)&$3F)
			bytes[count+4]=$80|(d&$3F)
			count+=5
			Continue
		Endif
	Next
	Return bytes.Resize(count)
End

Function UTF8Decode:Int[](bytes:Int[])
	Local data[]
	Local in
	Local out
	data=New Int[bytes.Length]
	While in<bytes.Length
		Local d=bytes[in]
		If d&$80=0
			in+=1
		Else If d&$E0=$C0
			d=((d&$1F)Shl 6) | (bytes[in+1]&$3F)
			in+=2
		Else If d&$F0=$E0
			d=((d&$F) Shl 12) | ((bytes[in+1]&$3F)Shl 6) | (bytes[in+2]&$3F)
			in+=3
		Else If d&$F8=$F0
			d=((d&$7) Shl 18) | ((bytes[in+1]&$3F)Shl 12) | ((bytes[in+2]&$3F)Shl 6) | (bytes[in+3]&$3F)
			in+=4
		Else If d&$FC=$F8
			d=((d&$3) Shl 24) | ((bytes[in+1]&$3F)Shl 18) | ((bytes[in+2]&$3F)Shl 12) | ((bytes[in+3]&$3F)Shl 6) | (bytes[in+4]&$3F)
			in+=5
		Else
			d=((d&$3) Shl 30) | ((bytes[in+1]&$3F)Shl 24) | ((bytes[in+2]&$3F)Shl 18) | ((bytes[in+3]&$3F)Shl 12) | ((bytes[in+4]&$3F)Shl 6) | (bytes[in+5]&$3F)
			in+=6
		Endif
		data[out]=d
		out+=1
	Wend
	Return data.Resize(out)
End Function

' base64 functions

Global MIME$="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
Global Dime:Int[]

Function Base64Encode$(bytes:Int[])
	Local n=bytes.Length
	Local buffer$

	For Local i=0 Until n Step 3
		Local b0,b1,b2,b24
		Local pad$
		b0=bytes[i]
		If i+1<n
			b1=bytes[i+1]
			If i+2<n
				b2=bytes[i+2]
			Else
				pad="="
			Endif
		Else
			pad="=="
		Endif
		b24=(b0 Shl 16) | (b1 Shl 8) | (b2)
		buffer+=String.FromChar( MIME[(b24 Shr 18)&63] )
		buffer+=String.FromChar( MIME[(b24 Shr 12)&63] )
		buffer+=String.FromChar( MIME[(b24 Shr 6)&63] )
		buffer+=String.FromChar( MIME[(b24)&63] )
		If pad buffer+=pad
	Next

	Return buffer
End

Function Base64Decode:Int[] (mime:String)


	'added by Eric (bitJericho) Canales
	'filter any characters not included in the base64 map, including spaces and newlines
	Local filtered$
	For Local mime_char = EachIn mime
		If MIME.Contains(String.FromChar(mime_char)) Or String.FromChar(mime_char) = "="
			filtered = filtered + String.FromChar(mime_char)
		EndIf
	Next
	mime = filtered
	'end addition
	
	If Not Dime
		Dime = New Int[256]
		For Local in = 0 To 63
			Dime[MIME[in]] = in
		Next
	Endif
	
	Local bytes:Int[]
	Local length=mime.Length
	Local pad
	Local i,p
	
	If mime[length-1]="="[0]
		pad=1
		If mime[length-2]="="[0]
			pad=2
		Endif
	Endif
	
	length=Int((length-pad)/4)*3
	bytes=New Int[length]
	
	While i<length
		Local b0=Dime[mime[p]]
		Local b1=Dime[mime[p+1]]
		Local b2=Dime[mime[p+2]]
		Local b3=Dime[mime[p+3]]
		Local b24=(b0 Shl 18)|(b1 Shl 12)|(b2 Shl 6)|b3
		bytes[i+0]=(b24 Shr 16)&255
		bytes[i+1]=(b24 Shr 8)&255
		bytes[i+2]=(b24)&255
		p+=4
		i+=3
	Wend
	If pad
		bytes = bytes.Resize(length - pad)
	EndIf
	


	Return bytes
End