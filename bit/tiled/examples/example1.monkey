#Rem
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#END

Strict

#MOJO_IMAGE_FILTERING_ENABLED=False

Import mojo.app
Import mojo.graphics
Import bit.tiled
Import mojo.asyncloaders
Import brl.filepath

Class Game Extends App
	Field looptime:Int

	Field tiled:Tiled<Image>

	'used for rendering the map	
	Field startX:Int, endX:Int, stepX:Int
	Field startY:Int, endY:Int, stepY:Int
	Field mapX:Int, mapY:Int
	Field posX:Int, posY:Int
	
	Method OnCreate:Int()
		'load a map
		tiled = New Tiled<Image>("monkey://data/other/perspective_walls.tmx")
		
		'load the images for all the tilesets
		For Local tileset:= EachIn tiled.Map.Tileset
			
			tileset.Image.ImageResource = LoadImage(ExtractDir(tiled.FilePath) + "/" + tileset.Image.Source)
			
			If tileset.Image.ImageResource = Null
				Error "Error loading file at location: " + ExtractDir(tiled.FilePath) + "/" + tileset.Image.Source
			EndIf

			'If tileset.Image.Trans <> ""
				'take care of transparent color on image
				'not really supported by mojo so not including any code here				
			'EndIf
					
			'some maps don't have a tilecount, so we have to figure it out in that case
			Local tilecount:Int = tileset.TileCount
			If tileset.TileCount = 0 ' if the tilecount is provided, then we can skip this and just load the exact number of tiles
				Local tcx:Int = tileset.Margin
				Local tcy:Int = tileset.Margin
				While tcy - tileset.Spacing < tileset.Image.ImageResource.Height() -tileset.Margin 'for y = startY to endY step stepY
			        While tcx - tileset.Spacing < tileset.Image.ImageResource.Width() -tileset.Margin 'for x = startX to endX step stepX
						tcx += tileset.TileWidth + tileset.Spacing
						tilecount += 1
					Wend
					tcy += tileset.TileHeight + tileset.Spacing
					tcx = tileset.Margin
				Wend
			EndIf
		
			'load the images for each tile from the tileset
			Local x:= tileset.Margin
			Local y:= tileset.Margin
			
			For Local i:Int = tileset.FirstGID To tileset.FirstGID + tilecount - 1
				tileset.Tile.Add(i, New Tiled_Tile<Image>())
				
				If x + tileset.Spacing + tileset.Margin + tileset.TileWidth - 1 > tileset.Image.ImageResource.Width()
					x = tileset.Margin
					y = y + tileset.Spacing + tileset.TileHeight
				End
				
				tileset.Tile.Get(i).Image.ImageResource = tileset.Image.ImageResource.GrabImage(x, y, tileset.TileWidth, tileset.TileHeight)
				tileset.Tile.Get(i).Image.ImageResource.SetHandle(0, tileset.TileHeight - 1) 'if a tile is overdrawn it should overdraw to the right and to the top, so we'll set the handle to the bottom left
				x = x + tileset.Spacing + tileset.TileWidth
			Next i
		Next
		
		'load the images for any image layers
		For Local imagelayer:= EachIn tiled.Map.ImageLayer
			imagelayer.Image.ImageResource = LoadImage(ExtractDir(tiled.FilePath) + "/" + imagelayer.Image.Source)
		Next
		
		'all done loading images
		
		SetUpdateRate 40
		
		
		Return Super.OnCreate()
	End
	
	Method OnUpdate:Int()

		Return 0
	End
	
    Method OnRender:Int()
		If tiled.Map.BackgroundColor <> ""
			Cls(HexToInt(tiled.Map.BackgroundColor[3 .. 5]), HexToInt(tiled.Map.BackgroundColor[5 .. 7]), HexToInt(tiled.Map.BackgroundColor[7 .. 9]))
		Else
			Cls(128, 128, 128) 'tiled default is 128,128,128
		EndIf
		
		DrawText(String(Millisecs() -looptime), 0, 0)
		looptime = Millisecs()
		
		If tiled.Map.Orientation = "orthogonal" 'the RenderOrder property is only honored for orthogonal projection by Tiled.
			Select tiled.Map.RenderOrder
				Case "right-down"
					startX = 0
					endX = tiled.Map.Width - 1
					startY = 0
					endY = tiled.Map.Height - 1
					stepX = 1
					stepY = 1
				Case "right-up"
					startX = 0
					endX = tiled.Map.Width - 1
					startY = tiled.Map.Height - 1
					endY = 0
					stepX = 1
					stepY = -1
				Case "left-down"
					startX = tiled.Map.Width - 1
					endX = 0
					startY = 0
					endY = tiled.Map.Height - 1
					stepX = -1
					stepY = 1
				Case "left-up"
					startX = tiled.Map.Width - 1
					endX = 0
					startY = tiled.Map.Height - 1
					endY = 0
					stepX = -1
					stepY = -1
				Default
					'default is right-down
					startX = 0
					endX = tiled.Map.Width - 1
					startY = 0
					endY = tiled.Map.Height - 1
					stepX = 1
					stepY = 1
			End
		Else 'default is right-down
			startX = 0
			endX = tiled.Map.Width - 1
			startY = 0
			endY = tiled.Map.Height - 1
			stepX = 1
			stepY = 1
		EndIf
		
		
		'loop through each map layer
	    For Local layer:= EachIn tiled.Map.Layer()
			mapX = startX
			mapY = startY

			If layer.Visible = 1 And layer.Opacity > 0
				Repeat

					Local gid:= layer.Data.Value(mapX, mapY, tiled.Map.Width, tiled.Map.Height)
                    If gid <> 0
					
						'find if need to flip the tile
						Local flipH:Int = gid & $80000000
						Local flipV:Int = gid & $40000000
						Local flipD:Int = gid & $20000000
												
						'remove the flags from the gid
						gid &= ~ ($80000000 | $40000000 | $20000000)
												
						'find the first tileset that can match the gid
						'tilesets must be sorted by FirstGID, which is part of the spec
						Local tileset:Tiled_Tileset<Image>
						For tileset = EachIn tiled.Map.Tileset.Backwards()
							If tileset.FirstGID <= gid
								Exit
							EndIf
						Next tileset
					
						If tileset.Tile.Get(gid) <> Null And tileset.Tile.Get(gid).Image <> Null
						
							If tiled.Map.Orientation = "staggered" Or tiled.Map.Orientation = "hexagonal"
								If tiled.Map.StaggerAxis = "X"
									posX = mapX * ( (tiled.Map.HexSideLength + tiled.Map.TileWidth) / 2)
									posY = mapY * tiled.Map.TileHeight
									If tiled.Map.StaggerIndex = "even" And mapX Mod 2 = 0
										posY = posY + (tiled.Map.TileHeight/2)
									ElseIf tiled.Map.StaggerAxis = "odd" And mapY Mod 2 = 1
										posY = posY + (tiled.Map.TileHeight / 2)
									EndIf
								ElseIf
									posX = mapX * tiled.Map.TileWidth
									posY = mapY * ( (tiled.Map.HexSideLength + tiled.Map.TileHeight) / 2)
									If tiled.Map.StaggerIndex = "even" And mapY Mod 2 = 0
										posX = posX + (tiled.Map.TileWidth / 2)
									ElseIf tiled.Map.StaggerIndex = "odd" And mapY Mod 2 = 1
										posX = posX + (tiled.Map.TileWidth / 2)
									EndIf
								EndIf
							ElseIf tiled.Map.Orientation = "isometric"
								posX = (mapX - mapY) * (tiled.Map.TileWidth / 2)
								posY = (mapX + mapY) * (tiled.Map.TileHeight / 2)

								'screen.x = (map.x - map.y) * TILE_WIDTH_HALF;
								'screen.y = (map.x + map.y) * TILE_HEIGHT_HALF;
							Else
								posY = mapY * tiled.Map.TileHeight
								posX = mapX * tiled.Map.TileWidth
							EndIf

							DrawImage(tileset.Tile.Get(gid).Image.ImageResource, tileset.TileOffset.X + layer.OffsetX + posX, tileset.TileOffset.Y + layer.OffsetY + posY)
							
						EndIf
                    EndIf
					
					'because of the many different render orders in Tiled, we have to have a complex
					'loop here. Basically this just increments the x and y variables based on the
					'render order we are using. Once we've looped through all the x and y coordinates
					'we exit					
					If mapY = endY And mapX = endX
						Exit
					Else
						'Print String(mapX) + ", " + String(mapY)
						If mapX = endX
							mapY += stepY
							mapX = startX
						Else
							mapX += stepX
						EndIf
					End
				
				Forever
			EndIf

	    Next
		Return Super.OnRender()
    End
End

Function Main:Int()
	New Game()
	Return 0
End

Function HexToInt:Int(value:String)
	Local intValue:Int = 0
	For Local i:Int = 0 To value.Length - 1
		Local value2:Int = value[i]
		If value2 >= 48 And value2 <= 57 Then
			intValue = intValue * 16 + value2 - 48
		ElseIf value2 >= 65 And value2 <= 70 Then
			intValue = intValue * 16 + value2 - 55
		End
	Next
	Return intValue
End